package com.agero.ncc.views;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.agero.ncc.R;

public class AttachmentRecyclerView extends RecyclerView {

    private float columnWidth;
    private GridLayoutManager manager;

    public AttachmentRecyclerView(Context context) {
        super(context);
        init(context);
    }

    public AttachmentRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        columnWidth = getResources().getDimension(R.dimen.add_attachment_image_width);
        if (columnWidth > 0) {
            manager = new GridLayoutManager(context, 1);
        }
        setLayoutManager(manager);
    }

    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (columnWidth > 0) {
            int spanCount = (int) Math.max(1, getMeasuredWidth() / columnWidth);
            manager.setSpanCount(spanCount);
        }
    }

}
