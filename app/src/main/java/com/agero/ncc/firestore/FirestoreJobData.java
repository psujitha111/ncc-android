package com.agero.ncc.firestore;

import com.agero.ncc.model.JobDetail;

public class FirestoreJobData {
    private static FirestoreJobData mFirestoreJobData;
    private JobDetail jobDetail;

    public JobDetail getJobDetail() {
        return jobDetail;
    }

    public void setJobDetail(JobDetail jobDetail) {
        this.jobDetail = jobDetail;
    }

    public boolean isFirestore() {
        return isFirestore;
    }

    public void setFirestore(boolean firestore) {
        isFirestore = firestore;
    }

    private boolean isFirestore;

    private FirestoreJobData(){

    }

    public static FirestoreJobData getInStance(){
        if(mFirestoreJobData == null){
            mFirestoreJobData = new FirestoreJobData();
        }
        return mFirestoreJobData;
    }


}
