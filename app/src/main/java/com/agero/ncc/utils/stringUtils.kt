package com.agero.ncc.utils

fun String.toCamelCase(separator: Char = '_', firstCapital: Boolean = true): String {
    val builder = StringBuilder()
    var capitalFlag = firstCapital
    for (c in this) {
        when (c) {
            separator -> capitalFlag = true
            else -> {
                builder.append(if (capitalFlag) Character.toUpperCase(c) else Character.toLowerCase(c))
                capitalFlag = false
            }
        }
    }
    return builder.toString()
}

internal fun String.toUPPER_CASE(): String {
    val builder = StringBuilder()
    for (c in this) {
        if (c.isUpperCase() && builder.isNotEmpty()) builder.append('_')
        builder.append(c.toUpperCase())
    }
    return builder.toString()
}