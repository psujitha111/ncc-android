package com.agero.ncc.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


public class UiUtils {


  public static void showKeyBoard(final View editText) {
    editText.requestFocus();
    editText.post(new Runnable() {
      @Override
      public void run() {
        Context context = editText.getContext();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
      }
    });
  }


  public static void hideKeyboard(Activity activity) {
    //Find the currently focused view, so we can grab the correct window token from it.
    View view = activity.getCurrentFocus();
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
      view = new View(activity);
    }
    hideKeyboard(view);
  }

  public static void hideKeyboard(final View view) {
    if (view != null) {
      view.post(new Runnable() {
        @Override
        public void run() {
          Context context = view.getContext();
          if(context instanceof Activity) {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
          }
          InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
          inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
      });
    }
  }


  public static void postRefresh(final SwipeRefreshLayout refreshLayout) {
    postRefresh(refreshLayout, true);
  }

  public static void postRefresh(final SwipeRefreshLayout refreshLayout, final boolean refreshing) {
    refreshLayout.post(new Runnable() {
      @Override
      public void run() {
        refreshLayout.setRefreshing(refreshing);
      }
    });
  }

  public static String getJobIdDisplayFormat(String jobId){
    if(!TextUtils.isEmpty(jobId) && jobId.length() == 12) {
      String s1 = jobId.substring(0, 3);
      String s2 = jobId.substring(3, 6);
      String s3 = jobId.substring(6, 9);

      return s1 + "-" + s2 + "-" + s3 + "-" + jobId.substring(9, 12);
    }else{
      return "";
    }
  }

}
