package com.agero.ncc.app;


import android.app.Application;
import android.arch.persistence.room.Room;

import com.agero.ncc.db.dao.AlertDao;
import com.agero.ncc.db.database.NccDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private NccDatabase nccDatabase;

    public RoomModule(Application mApplication){
        nccDatabase= Room.databaseBuilder(mApplication,NccDatabase.class,"ncc-db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    NccDatabase providesRoomDatabase(){
        return nccDatabase;
    }

    @Singleton
    @Provides
    AlertDao providesAlertDao(NccDatabase nccDatabase){
        return nccDatabase.getAlertDao();
    }
}
