package com.agero.ncc.firebasefunctions;

public class HttpsCallableResult{
    private final Object data;

    HttpsCallableResult(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }
}
