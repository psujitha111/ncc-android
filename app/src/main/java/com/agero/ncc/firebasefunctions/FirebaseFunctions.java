package com.agero.ncc.firebasefunctions;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.security.ProviderInstaller.ProviderInstallListener;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.functions.internal.Preconditions;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.Request.Builder;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseFunctions {
    private static final Map<String, FirebaseFunctions> instances = new HashMap();
    private static final TaskCompletionSource<Void> providerInstalled = new TaskCompletionSource();
    private static boolean providerInstallStarted = false;
    private OkHttpClient client = new OkHttpClient();
    private final Serializer serializer = new Serializer();
    private final ContextProvider contextProvider;
    private final String projectId;
    private final String region;
    private boolean useTestURL = false;
    private static Context mContext;

    @VisibleForTesting
    FirebaseFunctions(Context context, String projectId, String region, ContextProvider contextProvider) {
        this.contextProvider = (ContextProvider)Preconditions.checkNotNull(contextProvider);
        this.projectId = (String)Preconditions.checkNotNull(projectId);
        this.region = (String)Preconditions.checkNotNull(region);
        maybeInstallProviders(context);
    }

    private static void maybeInstallProviders(final Context context) {
        TaskCompletionSource var1 = providerInstalled;
        synchronized(providerInstalled) {
            if (providerInstallStarted) {
                return;
            }

            providerInstallStarted = true;
        }

        Runnable runnable = new Runnable() {
            public void run() {
                ProviderInstaller.installIfNeededAsync(context, new ProviderInstallListener() {
                    public void onProviderInstalled() {
                        providerInstalled.setResult(null);
                    }

                    public void onProviderInstallFailed(int i, Intent intent) {
                        Log.d("FirebaseFunctions", "Failed to update ssl context");
                        providerInstalled.setResult(null);
                    }
                });
            }
        };
        Handler handler = new Handler(context.getMainLooper());
        handler.post(runnable);
    }

    private static FirebaseFunctions getInstance(FirebaseApp app, String region) {
        Preconditions.checkNotNull(app, "You must call FirebaseApp.initializeApp first.");
        Preconditions.checkNotNull(region);
        ContextProvider provider = new FirebaseContextProvider(app);
        String projectId = app.getOptions().getProjectId();
        String key = projectId + "|" + region;
        Map var5 = instances;
        synchronized(instances) {
            FirebaseFunctions instance = (FirebaseFunctions)instances.get(key);
            if (instance == null) {
                mContext = app.getApplicationContext();
                instance = new FirebaseFunctions(app.getApplicationContext(), projectId, region, provider);
                instances.put(key, instance);
            }

            return instance;
        }
    }

    public static FirebaseFunctions getInstance(FirebaseApp app) {
        return getInstance(app, "us-central1");
    }

    public static FirebaseFunctions getInstance() {
        return getInstance(FirebaseApp.getInstance(), "us-central1");
    }

    public HttpsCallableReference getHttpsCallable(String name) {
        return new HttpsCallableReference(this, name);
    }

    @VisibleForTesting
    URL getURL(String function) {
        StringBuilder builder = new StringBuilder();
        if (this.useTestURL) {
            builder.append("http://10.0.2.2:5005/").append(this.projectId).append("/").append(this.region).append("/");
        } else {
            builder.append("https://").append(this.region).append("-").append(this.projectId).append(".cloudfunctions.net/");
        }

        builder.append(function);

        try {
            return new URL(builder.toString());
        } catch (MalformedURLException var4) {
            throw new IllegalStateException(var4);
        }
    }

    @VisibleForTesting
    void setUseTestURL(boolean value) {
        this.useTestURL = value;
    }

    Task<HttpsCallableResult> call(final String name, @Nullable final Object data) {
        return providerInstalled.getTask().continueWithTask(new Continuation<Void, Task<HttpsCallableContext>>() {
            public Task<HttpsCallableContext> then(@NonNull Task<Void> task) {
                return FirebaseFunctions.this.contextProvider.getContext();
            }
        }).continueWithTask(new Continuation<HttpsCallableContext, Task<HttpsCallableResult>>() {
            public Task<HttpsCallableResult> then(@NonNull Task<HttpsCallableContext> task) throws Exception {
                if (!task.isSuccessful()) {
                    return Tasks.forException(task.getException());
                } else {
                    HttpsCallableContext context = (HttpsCallableContext)task.getResult();
                    return FirebaseFunctions.this.call(name, data, context);
                }
            }
        });
    }

    private Task<HttpsCallableResult> call(String name, @Nullable Object data, HttpsCallableContext context) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        } else {
            URL url = this.getURL(name);
            Map<String, Object> body = new HashMap();
            Object encoded = this.serializer.encode(data);
            body.put("data", encoded);
            JSONObject bodyJSON = new JSONObject(body);
            //Timber.d("Request from Local FirebaseFunctions");
            MediaType contentType = MediaType.parse("application/json");
            RequestBody requestBody = RequestBody.create(contentType, bodyJSON.toString());
            Builder request = (new Builder()).url(url).post(requestBody);
            if (context.getAuthToken() != null) {
                request = request.header("Authorization", "Bearer " + context.getAuthToken());
            }

            if (context.getInstanceIdToken() != null) {
                request = request.header("Firebase-Instance-ID-Token", context.getInstanceIdToken());
            }

            final TaskCompletionSource<HttpsCallableResult> tcs = new TaskCompletionSource();

            client.setConnectTimeout(60, TimeUnit.SECONDS);
            client.setReadTimeout(60, TimeUnit.SECONDS);
            client.setWriteTimeout(120,TimeUnit.SECONDS);

            Call call = this.client.newCall(request.build());
            call.enqueue(new Callback() {
                public void onFailure(Request request, IOException e) {
                    tcs.setException(e);
                }

                public void onResponse(Response response) throws IOException {
                    FirebaseFunctionsException.Code code = FirebaseFunctionsException.Code.fromHttpStatus(response.code());
                    String body = null;
                    body = response.body().string();
                    FirebaseFunctionsException exception = FirebaseFunctionsException.fromResponse(code, body, FirebaseFunctions.this.serializer);
                    if (exception != null) {
                        tcs.setException(exception);
                    } else {
                        JSONObject bodyJSON;
                        FirebaseFunctionsException e;
                        try {
                            bodyJSON = new JSONObject(body);
                        } catch (JSONException var8) {
                            e = new FirebaseFunctionsException("Response is not valid JSON object.", FirebaseFunctionsException.Code.INTERNAL, (Object)null, var8);
                            tcs.setException(e);
                            return;
                        }

                        Object dataJSON = bodyJSON.opt("data");
                        if (dataJSON == null) {
                            dataJSON = bodyJSON.opt("result");
                        }

                        if (dataJSON == null) {
                            e = new FirebaseFunctionsException("Response is missing data field.", FirebaseFunctionsException.Code.INTERNAL, (Object)null);
                            tcs.setException(e);
                        } else {
                            HttpsCallableResult result = new HttpsCallableResult(FirebaseFunctions.this.serializer.decode(dataJSON));
                            tcs.setResult(result);
                        }
                    }
                }
            });
            return tcs.getTask();
        }
    }
}

