package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobCompleteFragment extends BaseFragment {

    @BindView(R.id.image_job_complete)
    ImageView mImageJobComplete;
    @BindView(R.id.text_job_complete)
    TextView mTextJobComplete;


    public static JobCompleteFragment newInstance() {
        JobCompleteFragment fragment = new JobCompleteFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_job_complete, fragment_content, true);
        ButterKnife.bind(this, view);
        return superView;
    }

}
