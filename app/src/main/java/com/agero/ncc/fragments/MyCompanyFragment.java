package com.agero.ncc.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.AssignDriverListAdapter;
import com.agero.ncc.adapter.CompanyContactsAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Facility;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Location;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.apptentive.android.sdk.model.ExtendedData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;

import static com.agero.ncc.utils.NccConstants.USER_STATUS_ACTIVE;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_BLOCKED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_DELETED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_INACTIVE;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_VERIFIED;
import static com.apptentive.android.sdk.model.ExtendedData.Type.location;

public class MyCompanyFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener, CompanyContactsAdapter.ChildItemClick {
    public boolean isPermissionDenied;
    HomeActivity mHomeActivity;
    Facility mFacility;
    @BindView(R.id.text_company_location)
    TextView mTextCompanyLocation;
    @BindView(R.id.text_company_address)
    TextView mTextCompanyAddress;
    @BindView(R.id.recycler_contact_details)
    RecyclerView mRecyclerContactDetails;
    ArrayList<Profile> mContactsList;
    HashMap<Profile, String> mUserIdProfileHashMap;
    private CompanyContactsAdapter mCompanyContactsAdapter;
    private int serviceUnFinished = 0;
    UserError mUserError;
    private String mSelectedMobileNumber;
    private long oldRetryTime = 0;
    private StickyHeaderDecoration mStickyHeaderDecoration;
    private DatabaseReference mFacilityRef, mContactRef, mDriverJobReference;
    private HashMap<String, Integer> mAssignedJobs = new HashMap<>();
    private View view;
    private PopupMenu mPopupMenu;

    public MyCompanyFragment() {

        // Intentionally empty
    }

    public static MyCompanyFragment newInstance() {
        MyCompanyFragment fragment = new MyCompanyFragment();
        return fragment;
    }


    PermissionListener callPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            Uri call = Uri.parse("tel:" + mSelectedMobileNumber);
            Intent surf = new Intent(Intent.ACTION_CALL, call);
            startActivity(surf);

        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    private ValueEventListener facilityEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mPopupMenu != null) {
                mPopupMenu.dismiss();
            }
            loadFacility(dataSnapshot);
            serviceUnFinished--;
            hideProgressIfBothCompleted();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("MyCompany Facility Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getFacilityDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private ValueEventListener companyContactChangeEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mPopupMenu != null) {
                mPopupMenu.dismiss();
            }
            mContactsList.clear();
            mUserIdProfileHashMap.clear();
            serviceUnFinished--;
            hideProgressIfBothCompleted();
            if (dataSnapshot.hasChildren()) {
                Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                loadUsersList(childList);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("MyCompany Contact Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getContactDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    ValueEventListener valueEventListenerForJoblist = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mPopupMenu != null) {
                mPopupMenu.dismiss();
            }
            mAssignedJobs.clear();
            loadCurrentJoblist(dataSnapshot);
            serviceUnFinished--;
            if (serviceUnFinished <= 0) {
                hideProgress();
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Assign Driver Job List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getJobListDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void loadCurrentJoblist(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren() && isAdded()) {
            Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
            for (DataSnapshot data : childList) {
                try {
                    JobDetail job = data.getValue(JobDetail.class);
                    if (mAssignedJobs.containsKey(job.getDispatchAssignedToId())) {
                        int count = mAssignedJobs.get(job.getDispatchAssignedToId());
                        mAssignedJobs.put(job.getDispatchAssignedToId(), ++count);
                    } else {
                        mAssignedJobs.put(job.getDispatchAssignedToId(), 1);
                    }

                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }

        }
    }

    private void loadUsersList(Iterable<DataSnapshot> childList) {

        for (DataSnapshot data : childList) {
            try {
                Profile contact = data.getValue(Profile.class);
                ArrayList<String> roles = (ArrayList<String>) contact.getRoles();
                if (roles != null && (!TextUtils.isEmpty(contact.getFirstName()) || !TextUtils.isEmpty(contact.getLastName()))) {
                    if (mHomeActivity.isLoggedInDriver()) {
                        if (!(roles.size() == 1 && roles.contains(NccConstants.USER_ROLE_DRIVER)
                                && contact.getStatus() != null
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_INACTIVE)
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_BLOCKED))
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_DELETED)) {
                            mContactsList.add(contact);
                            mUserIdProfileHashMap.put(contact, data.getKey());
                        }
                    } else {
                        if (contact.getStatus() != null
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_INACTIVE)
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_BLOCKED)
                                && !contact.getStatus().equalsIgnoreCase(USER_STATUS_DELETED)) {
                            mContactsList.add(contact);
                            mUserIdProfileHashMap.put(contact, data.getKey());
                        }

                    }
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
        if (mContactsList.size() > 0) {
            setAdapter(mRecyclerContactDetails);
        }

    }

    private void hideProgressIfBothCompleted() {
        if (serviceUnFinished <= 0) {
            hideProgress();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        view = inflater.inflate(R.layout.fragment_company, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        if (getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.showBottomBar();
        } else {
            mHomeActivity.hideBottomBar();
        }
        mHomeActivity.showToolbar(getString(R.string.title_facility));
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.hideSaveButton();
        mContactsList = new ArrayList<>();
        mUserIdProfileHashMap = new HashMap<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerContactDetails.setLayoutManager(linearLayoutManager);
        mRecyclerContactDetails.removeItemDecoration(new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        mUserError = new UserError();
        if (Utils.isNetworkAvailable()) {
            getFacilityDataFromFirebase();
            getContactDataFromFirebase();
            getJobListDataFromFirebase();
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
        return superView;
    }

    private void setAdapter(RecyclerView recyclerView) {
        if (isAdded()) {
            ArrayList<Profile> contacts = new ArrayList<>();
            Location location = new Location(0.0, 0.0);
            Profile contact = new Profile(null, null, "", "", "", "", null, null, null, false, null, location, null);

            if (mFacility != null && mFacility.getAddressList() != null && mFacility.getAddressList().size() > 0) {
                contact.setFirstName(mFacility.getBusinessName());
                contact.setLastName(mFacility.getAddressList().get(0).getAddress1() + "," + mFacility.getAddressList().get(0).getAddress2() + (mFacility.getAddressList().get(0).getAddress2().isEmpty() ? "" : ",") + mFacility.getAddressList().get(0).getCity() + "," + mFacility.getAddressList().get(0).getState() + "," + mFacility.getAddressList().get(0).getPostalCode());
                contact.setMobilePhoneNumber(mFacility.getDispatchPhone());
                contact.setLocation(new Location(mFacility.getAddressList().get(0).getLatitude(), mFacility.getAddressList().get(0).getLongitude()));

            }

            contacts.add(contact);

            if (mContactsList.size() > 0) {
                sortList(mContactsList);
                contacts.addAll(mContactsList);
            }


            mCompanyContactsAdapter = new CompanyContactsAdapter(getContext(), contacts, this);
            try {
                if (mStickyHeaderDecoration != null) {
                    recyclerView.removeItemDecoration(mStickyHeaderDecoration);
                }
                mStickyHeaderDecoration = new StickyHeaderDecoration(mCompanyContactsAdapter);
                recyclerView.addItemDecoration(mStickyHeaderDecoration, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            recyclerView.setAdapter(mCompanyContactsAdapter);
        }
    }


    private void sortList(ArrayList<Profile> mContactsList) {
        if (mContactsList != null && mContactsList.size() > 0) {
            Collections.sort(mContactsList, new Comparator<Profile>() {
                @Override
                public int compare(Profile profile, Profile profile1) {
                    if (profile.getRoles() != null && profile1.getRoles() != null) {
                        if (profile1.getRoles().size() == 1 && profile1.getRoles().contains(NccConstants.USER_ROLE_DRIVER) && profile.getRoles().size() == 1 && profile.getRoles().contains(NccConstants.USER_ROLE_DRIVER)) {
                            return 0;
                        } else if (profile.getRoles().size() == 1 && profile.getRoles().contains(NccConstants.USER_ROLE_DRIVER)) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }
                    return 0;
                }
            });
        }
    }

    private void getFacilityDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    serviceUnFinished++;
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    mFacilityRef = database.getReference(
                            "Facility/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                    //mFacilityRef.keepSynced(true);
                    mFacilityRef.addValueEventListener(facilityEventListener);

                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });


    }

    private void getContactDataFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    serviceUnFinished++;
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    mContactRef = database.getReference(
                            "Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                    //mContactRef.keepSynced(true);
                    mContactRef.addListenerForSingleValueEvent(companyContactChangeEventListener);
                    mContactRef.keepSynced(true);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getJobListDataFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    serviceUnFinished++;
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    mDriverJobReference = database.getReference("ActiveJobs/"
                            + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                    //mDriverJobReference.keepSynced(true);
                    mDriverJobReference.addValueEventListener(valueEventListenerForJoblist);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                mHomeActivity.tokenRefreshFailed();
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false).commit();
        if (isPermissionDenied) {
            isPermissionDenied = false;
            showPermissionDeniedDialog();
        }
    }

    private void loadFacility(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren()) {
            try {
                mFacility = dataSnapshot.getValue(Facility.class);

                if (mFacility != null) {
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", new Gson().toJson(mFacility));
                    mHomeActivity.mintlogEventExtraData("MyCompany Facility", extraDatas);
                }

                setAdapter(mRecyclerContactDetails);
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
    }


    /**
     * To show dialog for permanently denied permission.
     */
    public void showPermissionDeniedDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newOkDialog(
                getResources().getString(R.string.permission_denied_title),
                getResources().getString(R.string.permission_denied_camera_message));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getActivity().getPackageName(), null)));
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(),
                BasicDetailsFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onSave() {
        PopupMenu mPopupMenu = new PopupMenu(getActivity(), mHomeActivity.getCompanyOptionView());
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_company_popup, mPopupMenu.getMenu());

        mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.item_switch_company:
                        showCompanySelectionDialog();
                        break;
                    case R.id.item_add_company:
                        mHomeActivity.push(AddCompanyFragment.newInstance(), "Add company");
                        break;
                }
                return true;
            }
        });
        mPopupMenu.show();
    }

    private void showCompanySelectionDialog() {
        String[] arrayCompany = {"Lowe's Towing", "Lowe's Body shop and Towing"};
        AlertDialog.Builder builderCompanySelection = new AlertDialog.Builder(getActivity());
        builderCompanySelection.setTitle(getString(R.string.company_text_select_company));
        builderCompanySelection.setSingleChoiceItems(arrayCompany, 0, null);
        builderCompanySelection.setPositiveButton(getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builderCompanySelection.setNegativeButton(getString(R.string.cancel), null);
        builderCompanySelection.show();
    }

    @Override
    public void phoneNumberClicked(String phoneNumber) {
        this.mSelectedMobileNumber = phoneNumber;

        if (Utils.isCallSupported(mHomeActivity)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Dexter.isRequestOngoing()) {
                    Dexter.checkPermission(callPermissionListener, Manifest.permission.CALL_PHONE);
                }
            } else {
                Uri call = Uri.parse("tel:" + phoneNumber);
                Intent surf = new Intent(Intent.ACTION_CALL, call);
                startActivity(surf);
            }
        } else {
            mUserError.message = "Cannot make call";
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    @Override
    public void emailClicked(String email) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + email));
            intent.putExtra(Intent.EXTRA_SUBJECT, "NCC");
            intent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            //TODO smth
        }
    }

    @Override
    public void moreClicked(final Profile clickedProfile, int[] location) {


        if (isAdded()) {
            FrameLayout frameLayout = view.findViewById(R.id.temp_lay);
            frameLayout.removeAllViews();
            TextView textView = new TextView(mHomeActivity);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(1, 1);
            layoutParams.leftMargin = location[0];
            layoutParams.topMargin = location[1];
            textView.setLayoutParams(layoutParams);
            frameLayout.addView(textView);
            textView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    showPopup(clickedProfile, textView);
                    textView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

        }
    }

    private void showPopup(Profile clickedProfile, View textView) {
        if (isAdded()) {
            mPopupMenu = new PopupMenu(mHomeActivity, textView);
            mPopupMenu.getMenuInflater().inflate(R.menu.menu_call_onduty_popup, mPopupMenu.getMenu());

            if (clickedProfile.getOnDuty() != null && clickedProfile.getOnDuty()) {
                mPopupMenu.getMenu().findItem(R.id.item_switch_on_duty).setVisible(false);
            } else {
                mPopupMenu.getMenu().findItem(R.id.item_switch_off_duty).setVisible(false);
            }

            if (TextUtils.isEmpty(clickedProfile.getMobilePhoneNumber())) {
                mPopupMenu.getMenu().findItem(R.id.item_call_driver).setEnabled(false);
            } else {
                mPopupMenu.getMenu().findItem(R.id.item_call_driver).setEnabled(true);
            }

            mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                       /* case R.id.item_active_user:
                            mHomeActivity.updateProfileStatus(mUserIdProfileHashMap.get(clickedProfile), USER_STATUS_ACTIVE);
                            break;
                        case R.id.item_verify_user:
                            mHomeActivity.updateProfileStatus(mUserIdProfileHashMap.get(clickedProfile), USER_STATUS_VERIFIED);
                            break;*/
                        case R.id.item_call_driver:
                            phoneNumberClicked(clickedProfile.getMobilePhoneNumber());
                            break;
                        case R.id.item_switch_off_duty:
                            String clickedUserId = mUserIdProfileHashMap.get(clickedProfile);
                            if (mAssignedJobs.containsKey(clickedUserId)) {
                                mUserError.message = getString(R.string.alert_off_duty);
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                            } else {
                                if (Utils.isNetworkAvailable()) {
                                    showProgress();
                                    TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                                        @Override
                                        public void onRefreshSuccess() {
                                            hideProgress();
                                            if (clickedUserId != null && clickedProfile != null && clickedProfile.getEquipment() != null && clickedProfile.getEquipment().getEquipmentId() != null) {
                                                String equipmentId = clickedProfile.getEquipment().getEquipmentId();

                                                HashMap<String, String> extraDatas = new HashMap<>();
                                                extraDatas.put("duty_status", "Off Duty");
                                                mHomeActivity.mintlogEventExtraData("My Company Duty Change", extraDatas);

                                                mHomeActivity.uploadDuty(clickedUserId, false, equipmentId, false, new HomeActivity.DutyUpdateListener() {
                                                    @Override
                                                    public void onSuccess() {
                                                        getContactDataFromFirebase();
                                                    }

                                                    @Override
                                                    public void onFailure() {

                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onRefreshFailure() {
                                            hideProgress();
                                            if(isAdded()) {
                                                mHomeActivity.tokenRefreshFailed();
                                            }
                                        }
                                    });
                                } else {
                                    hideProgress();
                                    mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }

                            }

                            break;
                        case R.id.item_switch_on_duty:
                            String userId = mUserIdProfileHashMap.get(clickedProfile);
                            mHomeActivity.push(EquipmentSelectionFragment.newInstance(userId, false), getString(R.string.title_select_equipment));
                            break;
                    }
                    return true;
                }
            });
            mPopupMenu.show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (mContactRef != null) {
                mContactRef.removeEventListener(companyContactChangeEventListener);
            }
            if (mFacilityRef != null) {
                mFacilityRef.removeEventListener(facilityEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (mContactRef != null) {
                mContactRef.removeEventListener(companyContactChangeEventListener);
            }
            if (mFacilityRef != null) {
                mFacilityRef.removeEventListener(facilityEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mContactRef != null) {
                mContactRef.removeEventListener(companyContactChangeEventListener);
            }
            if (mFacilityRef != null) {
                mFacilityRef.removeEventListener(facilityEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
