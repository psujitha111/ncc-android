package com.agero.ncc.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.base.BaseHookHandler;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.events.EventType;
import co.chatsdk.core.events.NetworkEvent;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import co.chatsdk.core.types.AuthKeys;
import co.chatsdk.firebase.FirebaseEventHandler;
import co.chatsdk.firebase.wrappers.UserWrapper;
import co.chatsdk.ui.helpers.DialogUtils;
import co.chatsdk.ui.manager.InterfaceManager;
import co.chatsdk.ui.threads.ThreadSorter;
import co.chatsdk.ui.threads.ThreadsListAdapter;
import co.chatsdk.ui.utils.ToastHelper;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.agero.ncc.services.NCCBadgeUpdateService.isIndivualChat;
import static com.agero.ncc.services.NCCBadgeUpdateService.isTodayConversation;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_BLOCKED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_DELETED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_INACTIVE;

public class ChatHistoryFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {

    DatabaseReference myRef;
    @BindView(R.id.text_message)
    TextView mTextMessage;
    @BindView(R.id.text_message_description)
    TextView mTextMessageDescription;
    private UserError mUserError;
    private HomeActivity mHomeActivity;
    private String mLoginUserId;
    private ArrayList<String> mChatHistoryUserIds = new ArrayList<>();
    private ArrayList<Profile> mChatHistoryUserList = new ArrayList<>();
    private LinkedHashMap<String, Profile> mTotalUserHashMap = new LinkedHashMap<>();
    private LinkedHashMap<String, String> mTotalUserHashMapName = new LinkedHashMap<>();
    private LinkedHashMap<String, Profile> mNewUserHashMap = new LinkedHashMap<>();
    protected ThreadsListAdapter adapter;
    private List<Thread> mThreadList;
    public static boolean mIsTotalUserInserted;

    @BindView(R.id.recycler_chat_history)
    RecyclerView recyclerView;
    @BindView(R.id.create_new_chat)
    FloatingActionButton mCreateNewChat;
    LinearLayoutManager mLinearLayoutManager;
    private long oldRetryTime = 0;

    ValueEventListener mTotalUserValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            /*try {
                myRef.removeEventListener(mTotalUserValueEventListener);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            if (dataSnapshot.hasChildren()) {
                mTotalUserHashMap.clear();
                mTotalUserHashMapName.clear();
                Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                for (DataSnapshot data : childList) {
                    try {
                        Profile userProfile = data.getValue(Profile.class);
                        if (userProfile != null && userProfile.getStatus() != null
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_INACTIVE)
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_BLOCKED)
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_DELETED)) {

                            String userName = "";
                            if (!TextUtils.isEmpty(userProfile.getFirstName())) {
                                userName = userProfile.getFirstName().trim();
                            }
                            if (!TextUtils.isEmpty(userProfile.getLastName())) {
                                if (userName.length() > 0) {
                                    userName += " ";
                                }
                                userName += userProfile.getLastName().trim();
                            }

                            if(mHomeActivity.isLoggedInDriver() && !mHomeActivity.isUserOnlyDriver(userProfile.getRoles())){
                                createUserForProfile(data.getKey(), userProfile);
                                mTotalUserHashMap.put(data.getKey(), userProfile);
                                mTotalUserHashMapName.put(data.getKey(), userName);
                            }else if(!mHomeActivity.isLoggedInDriver()){
                                createUserForProfile(data.getKey(), userProfile);
                                mTotalUserHashMap.put(data.getKey(), userProfile);
                                mTotalUserHashMapName.put(data.getKey(), userName);
                            }


                           /* if (mHomeActivity.isUserDispatcherAndDriver(userProfile.getRoles()) || mHomeActivity.isUserDispatcherAndDriver()) {
                                createUserForProfile(data.getKey(), userProfile);
                                mTotalUserHashMap.put(data.getKey(), userProfile);
                                mTotalUserHashMapName.put(data.getKey(), userName);
                            } else if (mHomeActivity.isUserOnlyDriver(userProfile.getRoles()) && !mHomeActivity.isLoggedInDriver()) {
                                createUserForProfile(data.getKey(), userProfile);
                                mTotalUserHashMap.put(data.getKey(), userProfile);
                                mTotalUserHashMapName.put(data.getKey(), userName);
                            } else if (mHomeActivity.isLoggedInDriver() && !mHomeActivity.isUserOnlyDriver(userProfile.getRoles())) {
                                createUserForProfile(data.getKey(), userProfile);
                                mTotalUserHashMap.put(data.getKey(), userProfile);
                                mTotalUserHashMapName.put(data.getKey(), userName);
                            }*/

                        }
                    } catch (Exception e) {
                        mHomeActivity.mintLogException(e);
                    }
                }
            }
            if (mTotalUserHashMap.size() > 0 && adapter == null) {
                mIsTotalUserInserted = true;
                reloadData();
            }
            hideProgress();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Chat History Total User List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUsersList();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private HashMap<String, String> mJobStatus = new HashMap<>();
    private ArrayList<Thread> mIndividualThreads = new ArrayList<>();
    private ArrayList<DatabaseReference> mJobStatusReferences = new ArrayList<>();

    @Override
    public void reloadData() {
        if (isAdded() && mIsTotalUserInserted) {
            if (mIndividualThreads != null) {
                mIndividualThreads.clear();
            }

            boolean isJobChatPresent = false,isListenerRegistered = true;
            List<Thread> threads = NM.thread().getThreads(ThreadType.Private, false);
            Collections.sort(threads, new ThreadSorter());
            for (Thread thread :
                    threads) {
                if (isIndivualChat(thread)) {
                    mIndividualThreads.add(thread);
                } else if (isTodayConversation(thread) && thread.getName() != null) {
                    isJobChatPresent = true;
                    String jobId = getJobId(thread.getName());
                    if (!TextUtils.isEmpty(jobId) && !mJobStatus.containsKey(jobId)) {
                        isListenerRegistered = false;
                        mJobStatus.put(jobId, "");
                        mJobStatusReferences.add(addStatusListner(jobId));
                        showProgress();
                    }
                }
            }
            if (!isJobChatPresent) {
                ArrayList<Thread> individualThreads = new ArrayList<>();
                individualThreads.addAll(mIndividualThreads);
                loadAdapter(individualThreads);
            }else if(isListenerRegistered){
                loadAdapter(sortActiveChatList());
            }
        }
    }

    private void loadAdapter(List<Thread> list) {
        if(mThreadList != null){
            mThreadList.clear();
        }
        mThreadList = list;

        mHomeActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initList();
                if (adapter != null) {
                    adapter.clearData();
                    if (mThreadList.size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        mTextMessage.setVisibility(View.GONE);
                        mTextMessageDescription.setVisibility(View.GONE);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        mTextMessage.setVisibility(View.VISIBLE);
                        mTextMessageDescription.setVisibility(View.VISIBLE);
                    }
                    adapter.setmTotalUserHashMap(mTotalUserHashMapName);
                    adapter.updateThreads(mThreadList);
                }
            }
        });
    }

    ValueEventListener statusValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                String referenceLink = dataSnapshot.getRef().toString();
                Log.e("referenceLink", referenceLink);
                String referenceLink1 = referenceLink.replace("/currentStatus/statusCode", "");
                String jobId = referenceLink1.substring(referenceLink1.length() - 12, referenceLink1.length());
                String statusCode = (String) dataSnapshot.getValue();
                mJobStatus.put(jobId, statusCode);
                if (allJobStatusRead()) {
                    hideProgress();
                    loadAdapter(sortActiveChatList());

                }

            } catch (Exception e) {
              Timber.d("Exception"+e.getLocalizedMessage());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private List<Thread> sortActiveChatList() {
        ArrayList<Thread> finalList = new ArrayList<>();
        ArrayList<Thread> otherList = new ArrayList<>();
        for (Thread thread :
                NM.thread().getThreads(ThreadType.Private, false)) {
            if (!isIndivualChat(thread) && isTodayConversation(thread) && thread.getName() != null) {
                String jobId = getJobId(thread.getName());
                if (!(mJobStatus.get(jobId) == null || mJobStatus.get(jobId).equalsIgnoreCase("null"))) {
                    finalList.add(thread);
                } else {
                    otherList.add(thread);
                }
            }
        }
        finalList.addAll(mIndividualThreads);
        finalList.addAll(otherList);
        return finalList;
    }

    private boolean allJobStatusRead() {
        return !(mJobStatus.values().contains(""));
    }

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    private DatabaseReference addStatusListner(String jobId) {
        DatabaseReference databaseReference = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + jobId + "/currentStatus/statusCode").getRef();
        databaseReference.addValueEventListener(statusValueEventListener);
        return databaseReference;
    }

    private String getJobId(String name) {
        String jobId = "";
        if (name.toLowerCase().startsWith("job #")) {
            try {
                jobId = name.substring(5, name.length()).replace("-", "");
            } catch (Exception e) {
                Timber.d("not valid job id");
            }
        }
        return jobId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TokenManager.getInstance().validateToken(getActivity(), new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                  /*  NM.events().sourceOnMain()
                            .filter(NetworkEvent.filterPrivateThreadsUpdated())
                            .subscribe(networkEvent -> {
                                    reloadData();

                            });*/

                NM.events().sourceOnMain()
                        .filter(NetworkEvent.filterType(EventType.TypingStateChanged))
                        .subscribe(networkEvent -> {
                            if (adapter != null) {
                                adapter.setTyping(networkEvent.thread, networkEvent.text);
                                adapter.notifyDataSetChanged();
                            }
                        });
            }

            @Override
            public void onRefreshFailure() {
                ((HomeActivity) getActivity()).tokenRefreshFailed();
            }
        });

    }


    protected void initList() {

        // Create the adapter only if null, This is here so we wont
        // override the adapter given from the extended class with setAdapter.
        adapter = new ThreadsListAdapter(mHomeActivity);

        recyclerView.setLayoutManager(new LinearLayoutManager(mHomeActivity));
        recyclerView.setAdapter(adapter);
        recyclerView.setClickable(true);

        adapter.onClickObservable().subscribe(thread ->
                {
                    ChatActivity.toUserName = adapter.getNameForThread(thread);
                    ChatActivity.isFromJobDetail = false;
                    InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                }

        );

//        adapter.onLongClickObservable().subscribe(thread -> DialogUtils.showToastDialog(mHomeActivity, "", getResources().getString(co.chatsdk.ui.R.string.alert_delete_thread), getResources().getString(co.chatsdk.ui.R.string.delete),
//                getResources().getString(co.chatsdk.ui.R.string.cancel), null, () -> {
//
//                    deleteThread(thread);
//                    return null;
//
//                }));
    }

    private void deleteThread(Thread thread) {
        TokenManager.getInstance().validateToken(getActivity(), new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    NM.thread().deleteThread(thread)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new CompletableObserver() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onComplete() {
                                    if (isAdded()) {
                                        adapter.clearData();
                                        reloadData();
                                        ToastHelper.show(mHomeActivity, getString(co.chatsdk.ui.R.string.delete_thread_success_toast));
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    ToastHelper.show(mHomeActivity, getString(co.chatsdk.ui.R.string.delete_thread_fail_toast));
                                }
                            });
                }
            }

            @Override
            public void onRefreshFailure() {
                ((HomeActivity) getActivity()).tokenRefreshFailed();
            }
        });
    }

    public void clearData() {
        if (adapter != null) {
            adapter.clearData();
        }
    }

    public void startChat(String toUserId, Profile profile) {
        Thread presentThread = null;
        for (Thread thread :
                mThreadList) {
            for (User user :
                    thread.getUsers()) {
                if (user.getEntityID().equalsIgnoreCase(toUserId)) {
                    presentThread = thread;
                    break;
                }
            }
        }
        if (presentThread == null) {
            TokenManager.getInstance().validateToken(getActivity(), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        User user = createUserForProfile(toUserId, profile);
                        NM.thread().createThread("", user, NM.currentUser())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doFinally(() -> {
                                    dismissProgressDialog();
                                })
                                .subscribe(thread -> {
                                    if (isAdded()) {
                                        ChatActivity.isFromJobDetail = false;
                                        InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                                    }
                                }, throwable -> {
                                    ToastHelper.show(mHomeActivity, throwable.getLocalizedMessage());
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                }
            });
        } else {
            ChatActivity.isFromJobDetail = false;
            InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
        }
    }

    private User createUserForProfile(String toUserId, Profile profile) {
        User user = StorageManager.shared().createEntity(User.class);

        final UserWrapper userWrapper1 = UserWrapper.initWithModel(user);


        userWrapper1.getModel().setEntityID(toUserId);
        userWrapper1.getModel().setEmail(profile.getEmail());
        String userName = "";
        if (!TextUtils.isEmpty(profile.getFirstName())) {
            userName = profile.getFirstName().trim();
        }
        if (!TextUtils.isEmpty(profile.getLastName())) {
            if (userName.length() > 0) {
                userName += " ";
            }
            userName += profile.getLastName().trim();
        }

        userWrapper1.getModel().setName(userName);
        userWrapper1.getModel().setPhoneNumber(profile.getMobilePhoneNumber());
        userWrapper1.getModel().setAuthenticationType(2);
        userWrapper1.getModel().setLastOnline(new Date());
        userWrapper1.getModel().setLastUpdated(new Date());


        userWrapper1.getModel().update();

        return userWrapper1.getModel();
    }

    private void loadNewUser() {
        mNewUserHashMap.clear();
        for (Map.Entry<String, Profile> e1 : mTotalUserHashMap.entrySet()) {
            Profile user = e1.getValue();
            if (!mChatHistoryUserList.contains(user) && !mLoginUserId.equalsIgnoreCase(e1.getKey())) {
                mNewUserHashMap.put(e1.getKey(), user);
            }
        }
    }

    public ChatHistoryFragment() {
        // Intentionally empty
    }

    public static ChatHistoryFragment newInstance() {
        ChatHistoryFragment fragment = new ChatHistoryFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_chat_history, fragment_content, true);
        ButterKnife.bind(this, view);

        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showToolbar(getString(R.string.title_chat));
        mHomeActivity.showBottomBar();
        mHomeActivity.setOnToolbarSaveListener(this);
        mUserError = new UserError();
        mLinearLayoutManager = new LinearLayoutManager(mHomeActivity);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        mLoginUserId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");

        getUsersList();
        return superView;
    }

    private void getUsersList() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        myRef = database.getReference(
                                "Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                        myRef.keepSynced(true);
                        myRef.addValueEventListener(mTotalUserValueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    mHomeActivity.tokenRefreshFailed();
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void onDestroyView() {
        try {
            if (myRef != null) {
                myRef.removeEventListener(mTotalUserValueEventListener);
                if (mJobStatusReferences != null && mJobStatusReferences.size() > 0) {
                    for (DatabaseReference databaseReference :
                            mJobStatusReferences) {
                        databaseReference.removeEventListener(statusValueEventListener);
                    }
                    mJobStatusReferences.clear();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (myRef != null) {
                myRef.removeEventListener(mTotalUserValueEventListener);
                if (mJobStatusReferences != null && mJobStatusReferences.size() > 0) {
                    for (DatabaseReference databaseReference :
                            mJobStatusReferences) {
                        databaseReference.removeEventListener(statusValueEventListener);
                    }
                    mJobStatusReferences.clear();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(mTotalUserValueEventListener);
                if (mJobStatusReferences != null && mJobStatusReferences.size() > 0) {
                    for (DatabaseReference databaseReference :
                            mJobStatusReferences) {
                        databaseReference.removeEventListener(statusValueEventListener);
                    }
                    mJobStatusReferences.clear();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSave() {
        loadNewUser();
        ArrayList<Profile> mProfileList = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, Profile> e1 : mNewUserHashMap.entrySet()) {
            Profile profile = e1.getValue();
            profile.setUserId(e1.getKey());
            mProfileList.add(profile);
            i++;
        }
        ChatActivity.isFromJobDetail = false;
        mHomeActivity.push(ChatNewConversationFragment.newInstance(mProfileList, null), getString(R.string.new_conversation));
    }


}
