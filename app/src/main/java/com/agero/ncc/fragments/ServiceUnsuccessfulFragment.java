package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;

public class ServiceUnsuccessfulFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;

    Unbinder unbinder;
    /*String[] reason = {"Service attempt failed", "Wrong service requested", "Wrong equip. requested", "Customer refuese equip", "Need different equipment"};
    String[] serviceNeeded = {"Lock out", "Flat tire", "Fuel delivery", "Jumpstart", "Winch", "Tow", "Other"};
    String[] equipmentNeeded = {"Roadside service truck", "Low clearance truck", "Light duty wheel lift", "Light duty flatbed", "Medium duty wheel lift", "Medium duty flatbed", "Meidum duty under-reach"};
    */
    @BindView(R.id.text_service_unsuccessful_message)
    TextView mTextServiceUnsuccessfulMessage;
    @BindView(R.id.text_service_unsuccess_reason)
    TextView mTextServiceUnsuccessReason;
    @BindView(R.id.spinner_service_unsuccess_reason)
    Spinner mSpinnerServiceUnsuccessReason;
    @BindView(R.id.view_border_service_unsucces_reason)
    View mViewBorderServiceUnsuccesReason;
    @BindView(R.id.text_service_unsuccess_service_needed)
    TextView mTextServiceUnsuccessServiceNeeded;
    @BindView(R.id.spinner_service_unsuccess_service_needed)
    Spinner mSpinnerServiceUnsuccessServiceNeeded;
    @BindView(R.id.view_border_service_unsucces_service_needed)
    View mViewBorderServiceUnsuccesServiceNeeded;
    @BindView(R.id.text_service_unsuccess_equipment_needed)
    TextView mTextServiceUnsuccessEquipmentNeeded;
    @BindView(R.id.spinner_service_unsuccess_equipment_needed)
    Spinner mSpinnerServiceUnsuccessEquipmentNeeded;
    @BindView(R.id.view_border_service_unsuccess_equipment_needed)
    View mViewBorderServiceUnsuccessEquipmentNeeded;


    public ServiceUnsuccessfulFragment() {
    }

    public static ServiceUnsuccessfulFragment newInstance() {
        ServiceUnsuccessfulFragment fragment = new ServiceUnsuccessfulFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_service_unsuccessful, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.showToolbar(getString(R.string.title_service_unsuccessful));
        List<String> listReason = Arrays.asList(getResources().getStringArray(R.array.array_service_unsuccessful_reason));
        List<String> listServiceNeeded = Arrays.asList(getResources().getStringArray(R.array.array_service_unsuccessful_service_needed));
        List<String> listEquipmentNeeded = Arrays.asList(getResources().getStringArray(R.array.array_service_unsuccessful_equipment_needed));

        mTextServiceUnsuccessServiceNeeded.setVisibility(View.GONE);
        mSpinnerServiceUnsuccessServiceNeeded.setVisibility(View.GONE);
        mViewBorderServiceUnsuccesServiceNeeded.setVisibility(View.GONE);

        mTextServiceUnsuccessEquipmentNeeded.setVisibility(View.GONE);
        mSpinnerServiceUnsuccessEquipmentNeeded.setVisibility(View.GONE);
        mViewBorderServiceUnsuccessEquipmentNeeded.setVisibility(View.GONE);


        HintSpinner<String> hintSpinnerReason =new HintSpinner<>(mSpinnerServiceUnsuccessReason, new HintAdapter<String>(getActivity(),"Select", listReason),
                new HintSpinner.Callback<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                mTextServiceUnsuccessServiceNeeded.setVisibility(View.VISIBLE);
                mSpinnerServiceUnsuccessServiceNeeded.setVisibility(View.VISIBLE);
                mViewBorderServiceUnsuccesServiceNeeded.setVisibility(View.VISIBLE);
            }
        });
        hintSpinnerReason.init();


        HintSpinner<String> hintSpinnerServiceNeeded =new HintSpinner<>(mSpinnerServiceUnsuccessServiceNeeded, new HintAdapter<String>(getActivity(),"Select", listServiceNeeded),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {

                        mTextServiceUnsuccessEquipmentNeeded.setVisibility(View.VISIBLE);
                        mSpinnerServiceUnsuccessEquipmentNeeded.setVisibility(View.VISIBLE);
                        mViewBorderServiceUnsuccessEquipmentNeeded.setVisibility(View.VISIBLE);

                    }
                });
        hintSpinnerServiceNeeded.init();
        HintSpinner<String> hintSpinnerEquipmentNeeded =new HintSpinner<>(mSpinnerServiceUnsuccessEquipmentNeeded, new HintAdapter<String>(getActivity(),"Select", listEquipmentNeeded),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                    }
                });
        hintSpinnerEquipmentNeeded.init();
        return superView;
    }


    @Override
    public void onSave() {

    }

}
