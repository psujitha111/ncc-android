package com.agero.ncc.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.VehiclePhotoAdapter;
import com.agero.ncc.views.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class VehicleConditionPhotoFragment extends CameraBaseFragment implements HomeActivity.ToolbarSaveListener {
    public ArrayList<Uri> mPhotoUriList = new ArrayList<>();
    HomeActivity mHomeActivity;
    VehiclePhotoAdapter mVehiclePhotoAdapter;
    @BindView(R.id.recycler_vehicle_condition_photo)
    RecyclerView mRecyclerVehicleCondition;
    Unbinder unbinder;
    public static boolean isDocument;

    public VehicleConditionPhotoFragment() {
        // Intentionally empty
    }

    public static VehicleConditionPhotoFragment newInstance() {
        VehicleConditionPhotoFragment fragment = new VehicleConditionPhotoFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_vehicle_condition_photo, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);

        mPhotoUriList.add(Uri.parse(""));

        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_photos));
        mHomeActivity.setOnToolbarSaveListener(this);
        if(getArguments()!=null){
            isDocument = getArguments().getBoolean("isDocument");
        }
        mRecyclerVehicleCondition.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO);
                }
            }
        }));
        setAdapter(mPhotoUriList);
        return superView;
    }

    /**
     * To set adapter for recycler_vehicle_condition_photo
     * @param mPhotoLists array list of Uri.
     */
    private void setAdapter(ArrayList mPhotoLists) {
        mVehiclePhotoAdapter =
                new VehiclePhotoAdapter(getActivity(), mPhotoLists);
        mRecyclerVehicleCondition.setAdapter(mVehiclePhotoAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                mPhotoUriList.addAll(onMultipleSelectFromGalleryResult(data));
                setAdapter(mPhotoUriList);
            } else if (requestCode == REQUEST_CAMERA) {
                mPhotoUriList.add(onCaptureImageResult(data));
                setAdapter(mPhotoUriList);
            }else if (requestCode == PDF_FILE) {
                if(data.getData().toString().contains(".pdf")){
//                    mPhotoUriList.add(generateImageFromPdf(Uri.parse(data.getData().toString())));
                    setAdapter(mPhotoUriList);
                }
            }
        }
    }

    @Override
    public void onSave() {
        getActivity().onBackPressed();
    }
}
