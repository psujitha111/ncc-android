package com.agero.ncc.fragments;


import android.Manifest;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.FileUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.views.AddProfileBottomSheetDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Base Fragment to handle Camera events.
 */
public class CameraBaseFragment extends BaseFragment {
    public static final int SHOW_BOTTOM_SHEET_CREATE_ACCOUNT = 1;
    public static final int SHOW_BOTTOM_SHEET_BASIC_DETAILS = 2;
    public static final int SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO = 3;
    public String mCurrentPhotoPath;
    public static final String BUNDLE_KEY = "BottomSheet";
    public final static String FOLDER = Environment.getExternalStorageDirectory() + "/PDF";
    public boolean isPermissionDenied;
    public int REQUEST_CAMERA = 1;
    public int REQUEST_CAMERA_VIDEO = 4;
    public int SELECT_FILE = 2;
    public int PDF_FILE = 3;
    //    public int REQUEST_CAMERA_AND_VIDEO = 5;
    public boolean isVideoCapture;
    //    public boolean isPhotoVideoCapture;
    BaseActivity mWelcomeActivity;
    private Uri fileUri;
    //    Runtime Permission listener to camera.
    PermissionListener cameraPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {

            if (isAdded()) {
                if (isVideoCapture) {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA_VIDEO);
                } else {
                    openPhotoIntent();
                }
            }


        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    private void openPhotoIntent() {
        if (mShowBottomSheet == SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    Timber.d("unable to create file");
                }
                if (photoFile != null) {
                    Uri photoUri = FileProvider.getUriForFile(getActivity(),
                            "com.agero.ncc.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } else {
                Timber.d("unable to open camera");
            }
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "" + System.currentTimeMillis();
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    //    Runtime Permission listener to access external storage.
    PermissionListener externalPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            if (isAdded()) {
                createPhotoChooser();
            }
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mWelcomeActivity = (BaseActivity) getActivity();
    }

    private void createPhotoChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("com.agero.ncc.fragments.VehicleReportFragment");
        if (fragment != null && fragment.isVisible()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            }
            String[] mimeTypes = {"image/jpeg", "video/mp4", "application/pdf"};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                intent.setType("*/*");
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, SELECT_FILE);
            } else {
                intent.setType("image/jpeg");
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            }
        } else {
            intent.setType("image/jpeg");
            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
        }
    }


    /**
     * To show dialog for permanently denied permission.
     */
    public void showPermissionDeniedDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newOkDialog(getResources().getString(R.string.permission_denied_title), getResources().getString(R.string.permission_denied_camera_message));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getActivity().getPackageName(), null)));
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), CameraBaseFragment.class.getClass().getCanonicalName());
    }

    /**
     * To get bitmap from single selected image.
     *
     * @param data Intent.
     * @return Bitmap.
     */
    @SuppressWarnings("deprecation")
    public String onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        String path = "";
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bm, "images", null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    /**
     * To get list of Uri from multi-selected image.
     *
     * @param data Intent.
     * @return ArrayList of Uri.
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public ArrayList<Uri> onMultipleSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
        if (data != null) {
            if (data.getClipData() != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ClipData mClipData = data.getClipData();
                for (int i = 0; i < mClipData.getItemCount(); i++) {
                    ClipData.Item item = mClipData.getItemAt(i);
                    Uri uri = item.getUri();
                    mArrayUri.add(uri);
                }
            } else {
                Uri selectedMediaUri = data.getData();
                if (FileUtils.getFileType(selectedMediaUri.getPath()) == FileUtils.FileType.VIDEO) {

                    String selectedVideoFilePath = FileUtils.getPath(this.getContext(), selectedMediaUri);
                    mArrayUri.add(selectedMediaUri);
                    ThumbnailUtils.createVideoThumbnail(selectedVideoFilePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                } else if (FileUtils.getFileType(selectedMediaUri.getPath()) == FileUtils.FileType.IMAGE) {
                    mArrayUri.add(selectedMediaUri);
                } else {

                }
            }
        }
        return mArrayUri;
    }

    public Uri compressImage(Uri uri) {
        Bitmap original = null;
        try {
            original = getBitmapFromPath(uri.getPath());
            if (original == null) {
                original = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            original.compress(Bitmap.CompressFormat.PNG, 100, out);
            original.recycle();
            Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), decoded, "Title", null);
            uri = Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uri;
    }


    private Bitmap getBitmapFromPath(String photoPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(photoPath, options);
    }
    private void storeBitmapToPath(File file,Bitmap bitmap){
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void deleteFile(String path) {
        try {
            File dir = new File(path);
            if (dir != null && dir.isDirectory()) {
                deleteFile(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public boolean deleteFile(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {

                boolean success = deleteFile(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }


    public Uri onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getExternalFilesDir(null),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.fromFile(destination);
    }

    private int mShowBottomSheet;

    /**
     * To show bottom sheet with options to select camera or gallery.
     *
     * @param showBottomSheet
     */
    public void showPhotoSelectionBottomSheet(int showBottomSheet) {
        mShowBottomSheet = showBottomSheet;
        final AddProfileBottomSheetDialog bottomSheetDialog = AddProfileBottomSheetDialog.getInstance();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY, showBottomSheet);
        bottomSheetDialog.setArguments(bundle);
        bottomSheetDialog.show(getFragmentManager(), "Custom Bottom Sheet");
        bottomSheetDialog.setSharDialogClickListener(new AddProfileBottomSheetDialog.SharProfileDialogClickListener() {
            @Override
            public void onTakePhotoClick() {
//                isPhotoVideoCapture = false;
                isVideoCapture = false;
//                To handle runtime permission for above Marshmallow version.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Dexter.isRequestOngoing()) {
                        Dexter.checkPermission(cameraPermissionListener, Manifest.permission.CAMERA);
                    }
                } else {
                    openPhotoIntent();
                }
                bottomSheetDialog.dismiss();
            }

            @Override
            public void onTakeVideoClick() {
//                isPhotoVideoCapture = false;
                isVideoCapture = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Dexter.isRequestOngoing()) {
                        Dexter.checkPermission(cameraPermissionListener, Manifest.permission.CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                }
                bottomSheetDialog.dismiss();
            }

            @Override
            public void onChoosePhotoClick() {
                isVideoCapture = false;
//                isPhotoVideoCapture = false;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Dexter.isRequestOngoing()) {
                        Dexter.checkPermission(externalPermissionListener, Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                } else {
                    createPhotoChooser();
                }
                bottomSheetDialog.dismiss();
            }

            @Override
            public void onDeletePhotoClick() {
                deleteAlertDialog();

                bottomSheetDialog.dismiss();
            }

//            @Override
//            public void onTakePhotoVideoClick() {
//                isVideoCapture = false;
////                isPhotoVideoCapture = true;
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!Dexter.isRequestOngoing()) {
//                        Dexter.checkPermission(cameraPermissionListener, Manifest.permission.CAMERA);
//                    }
//                } else {
//                     photoVideoChooser();
//                }
//                bottomSheetDialog.dismiss();
//            }
        });
    }

//    private void photoVideoChooser() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
//        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
//        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
//        contentSelectionIntent.setType("*/*");
//        Intent[] intentArray = new Intent[]{takePictureIntent,takeVideoIntent};
//        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
//        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Choose an action");
//        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
//        startActivityForResult(chooserIntent, REQUEST_CAMERA_AND_VIDEO);

//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, REQUEST_CAMERA_AND_VIDEO);
//    }

    private void deleteAlertDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog("", Html.fromHtml("<big>" + getResources().getString(R.string.basic_alert_delete_text) + "</big>")
                , Html.fromHtml("<b>" + getString(R.string.basic_alert_delete_button) + "</b>"), Html.fromHtml("<b>" + getString(R.string.basic_alert_delete_cancel_button) + "</b>"));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (isAdded()) {
                    if (!mWelcomeActivity.isFinishing()) {
                        if (i == -1) {
                            mEditor.remove(NccConstants.CAMERA_IMAGE_PREF_KEY).commit();
                            if (mWelcomeActivity instanceof HomeActivity) {
                                ((HomeActivity) mWelcomeActivity).push(EditProfileFragment.newInstance());
                            } else {
                                ((WelcomeActivity) mWelcomeActivity).pushFragment(EditProfileFragment.newInstance());
                            }
                        }
                    }
                    dialogInterface.dismiss();
                }
            }
        });
        alert.show(getFragmentManager().beginTransaction(), CameraBaseFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPermissionDenied) {
            isPermissionDenied = false;
            showPermissionDeniedDialog();
        }
    }

}
