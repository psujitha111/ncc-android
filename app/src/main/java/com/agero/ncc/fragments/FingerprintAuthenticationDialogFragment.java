/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.agero.ncc.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.NccConstants;
import com.github.ajalt.reprint.core.AuthenticationFailureReason;
import com.github.ajalt.reprint.core.AuthenticationListener;
import com.github.ajalt.reprint.core.Reprint;

import javax.inject.Inject;

/**
 * A dialog which uses fingerprint APIs to authenticate the user, and falls back to password
 * authentication if fingerprint is not available.
 */
public class FingerprintAuthenticationDialogFragment extends DialogFragment {

    @Inject
    public SharedPreferences mPrefs;
    private FingerPrintCallBackListener fragmentCallbackListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not create a new Fragment when the Activity is re-created such as orientation changes.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        NCCApplication.getContext().getComponent().inject(this);
        getDialog().setTitle(getString(R.string.label_sigin_in));
        View v = inflater.inflate(R.layout.finger_print_dialog, container, false);
        ImageView fingerprint_icon = (ImageView) v.findViewById(R.id.fingerprint_icon);
        TextView fingerprint_status = (TextView) v.findViewById(R.id.fingerprint_status);
        Button cancel_button = (Button) v.findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dismiss();
                    Reprint.cancelAuthentication();
                }catch (Exception e){

                }
            }
        });

        Reprint.authenticate(new AuthenticationListener() {
                public void onSuccess(int moduleTag) {

                    fingerprint_icon.setImageResource(R.drawable.ic_fingerprint_success);
                    fingerprint_status.setText(fingerprint_status.getResources().getString(R.string.fingerprint_success));
                    fingerprint_status.setTextColor(ContextCompat.getColor(fingerprint_status.getContext(), R.color.finger_print_success_color));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(isAdded() && isVisible()) {
                                try {
                                    dismiss();
                                    Reprint.cancelAuthentication();
                                }catch (Exception e){

                                }
                            }
                            if (fragmentCallbackListener != null) {
                                fragmentCallbackListener.onSuccess();
                            }
                        }
                    }, 1000);
                }

                public void onFailure(AuthenticationFailureReason failureReason, boolean fatal,
                                      CharSequence errorMessage, int moduleTag, int errorCode) {
                    if(errorCode == 7){
                        fingerprint_status.setText(R.string.finger_print_error_message);
                    }else{
                        fingerprint_status.setText(errorMessage);
                    }
                }
            });
        return v;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Reprint.cancelAuthentication();
    }

    public void setFragmentCallbackLister(FingerPrintCallBackListener fragmentCallbackLister) {
        this.fragmentCallbackListener = fragmentCallbackLister;
    }

    public interface FingerPrintCallBackListener{
        void onSuccess();
        void onFailure();
    }

}
