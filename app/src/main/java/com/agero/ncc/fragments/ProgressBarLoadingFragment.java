package com.agero.ncc.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.agero.ncc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressBarLoadingFragment extends BaseFragment {
    @BindView(R.id.progress_bar)
    ProgressBar mprogressBar;

    public static ProgressBarLoadingFragment newInstance() {
        ProgressBarLoadingFragment fragment = new ProgressBarLoadingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Handler handler = new Handler();

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_progress_bar_loading, fragment_content, true);
        ButterKnife.bind(this, view);

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    public void run() {
                        mprogressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).start();
        return superView;
    }
}

