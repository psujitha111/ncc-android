package com.agero.ncc.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.CreateProfileActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.UserProfile;
import com.agero.ncc.utils.UserProfileStorage;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.requests.RequestActivity;
import com.zendesk.sdk.support.SupportActivity;
import com.zendesk.util.StringUtils;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatActivity;


/**
 * A placeholder fragment containing a simple view.
 */
public class ZendeskHelpFragment extends BaseFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ZendeskHelpFragment newInstance(int sectionNumber) {
        ZendeskHelpFragment fragment = new ZendeskHelpFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ZendeskHelpFragment() {
        // Intentionally empty
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View rootView = inflater.inflate(R.layout.zendesk_fragment_main, fragment_content, true);
        final Context ctx = getActivity().getApplicationContext();


        HomeActivity mHomeActivity = (HomeActivity) getActivity();
        if(getResources().getBoolean(R.bool.isTablet)){
            mHomeActivity.showBottomBar();
        } else {
            mHomeActivity.hideBottomBar();
        }
        mHomeActivity.showToolbar(getString(R.string.title_help));

        rootView.findViewById(R.id.fragment_main_btn_knowledge_base).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new com.zendesk.sdk.support.SupportActivity.Builder().show(getActivity());
            }
        });

        rootView.findViewById(R.id.fragment_main_btn_contact_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContactZendeskActivity.class);
                startActivity(intent);
            }
        });

        rootView.findViewById(R.id.fragment_main_btn_my_tickets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RequestActivity.class);
                startActivity(intent);
            }
        });

        rootView.findViewById(R.id.fragment_main_btn_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                PreChatForm build = new PreChatForm.Builder()
                        .name(PreChatForm.Field.REQUIRED)
                        .email(PreChatForm.Field.REQUIRED)
                        .phoneNumber(PreChatForm.Field.OPTIONAL)
                        .message(PreChatForm.Field.OPTIONAL)
                        .build();

                ZopimChat.SessionConfig department = new ZopimChat.SessionConfig()
                        .preChatForm(build)
                        .department("The date");

                ZopimChatActivity.startActivity(getActivity(), department);
            }
        });

        return superView;
    }

    class AuthOnClickWrapper implements View.OnClickListener {

        private View.OnClickListener mOnClickListener;
        private UserProfileStorage mUserProfileStorage;
        private Context mContext;

        public AuthOnClickWrapper(View.OnClickListener onClickListener, Context context){
            this.mOnClickListener = onClickListener;
            this.mUserProfileStorage = new UserProfileStorage(context);
            this.mContext = context;
        }

        @Override
        public void onClick(View v) {
            final UserProfile profile = mUserProfileStorage.getProfile();

            if(StringUtils.hasLength(profile.getEmail())){
                mOnClickListener.onClick(v);
            }else{
               showDialog();
            }
        }

        private void showDialog(){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.dialog_auth_title)
                    .setPositiveButton(R.string.dialog_auth_positive_btn, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(mContext, CreateProfileActivity.class));
                        }
                    })
                    .setNegativeButton(R.string.dialog_auth_negative_btn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Intentionally empty
                        }
                    });
            builder.create().show();
        }
    }
}
