package co.chatsdk.ui.threads;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import co.chatsdk.ui.R;

/**
 * Created by benjaminsmiley-andrews on 07/06/2017.
 */

public class ThreadViewHolder extends RecyclerView.ViewHolder {

    public TextView nameTextView;
    public TextView dateTextView;
    public TextView lastMessageTextView;
    public TextView mName;
    public SimpleDraweeView imageView;
    public ImageView mImageGroupChat;
    //public View indicator;

    public ThreadViewHolder(View itemView) {
        super(itemView);

        nameTextView = itemView.findViewById(R.id.chat_sdk_txt);
        lastMessageTextView = itemView.findViewById(R.id.txt_last_message);
        dateTextView = itemView.findViewById(R.id.txt_last_message_date);
        imageView = itemView.findViewById(R.id.img_thread_image);
        mName = itemView.findViewById(R.id.text_chat_flname);
        mImageGroupChat = itemView.findViewById(R.id.image_private_group);
      //  indicator = itemView.findViewById(R.id.chat_sdk_indicator);

    }

   /* public void showUnreadIndicator(){
        indicator.setVisibility(View.VISIBLE);
    }

    public void hideUnreadIndicator(){
        indicator.setVisibility(View.GONE);
    }
*/



}